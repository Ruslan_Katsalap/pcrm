$(document).on("click", ".company_btn", function(e){
	e.preventDefault()
	$('.toggle_menu').removeClass("sub_menu_trans_hide")
	$('.toggle_menu_content').show(10)
})

$(document).on("click", '.hide_sub_menu_btn', function(e){
	e.preventDefault()
	$('.toggle_menu').addClass("sub_menu_trans_hide")
	$('.toggle_menu_content').hide("fast")
})

$(function(){
	$( ".kanban_block" ).draggable();
	$( ".kanban_blocks_group" ).droppable({
		accept: ".kanban_block",
	    drop: function( event, ui ) {
	       var droppable = $(this);
	       var draggable = ui.draggable;
	       // Move draggable into droppable
	       draggable.appendTo(droppable);
	        draggable.css({top: '0px', left: '0px'});
	    } 
	});
});

$(document).on("click", '.show_menu_btn, .sand_btn', function(e){
	e.preventDefault()
	var screen_w = $(window).width()
	if(screen_w > 992) return

	$(".left_nav").toggle()
	$('.toggle_menu').addClass("sub_menu_trans_hide")
	$('.toggle_menu_content').hide("fast")
})

$(document).on("click", '.sidebar_btn', function(e){
	e.preventDefault()
	var type = $(this).data("type")
	if(type !== "company"){
		$('.toggle_menu').addClass("sub_menu_trans_hide")
		$('.toggle_menu_content').hide("fast")
	}
	$('section').hide()
	if(type == "dash"){
		$('#dashboard').show()
	} else if(type == "task"){
		$('#kanban').show()
	} else if(type == "backlog"){
		$('#backlog').show()
	} else if(type == "company"){
		$('#candidate, #job').show()
	} else if(type == "department"){
		$('#department, #add_dep').show()
	}
})